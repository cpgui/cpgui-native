# cpgui-native

Use other languages like java to create a module.

## Add connectors

* Open the `config.json`.
* Add this to the connectors:
    ````json
    {
        "ip":"localhost",
        "port":"85756",
        "token":""
    }
    ```
* Set the ip
* Set the port
* Create a token and add this to this config and to the config from the connector.
