# frozen_string_literal: true

require_relative './console.rb'
require_relative './info.rb'
require_relative './module.rb'

# Main module for the cpgui
class NativeModule
  include CPGUI::AppModule

  def on_enable
    @config = CPGUI::Configuration.new(File.join(__dir__, 'config.json'))
    language_dir = File.join(__dir__, 'language')
    @language = CPGUI::Language.new(@config.get('language'), language_dir)
    send_message @language.get('general', 'loading')
    register_commands
    send_message @language.get('general', 'loaded')
  end

  def on_disable
    send_message @language.get('general', 'unloading')
    send_message @language.get('general', 'unloaded')
  end

  def prefix
    @language.get('prefix')
  end

  def help(_args)
    @language.get('help')
  end
end
